package com.zb.controller;

import com.zb.pojo.Tour_route;
import com.zb.service.Tour_routeService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@CrossOrigin
public class Tour_routeController {
    @Resource
    Tour_routeService service;
    @RequestMapping("/show")
    public List<Tour_route> getTour_route(){
        return  service.getTour_route();
    }
    @RequestMapping("/add")
    public int getAdd(Tour_route tour_route){
        return  service.getAdd(tour_route);
    }
    @RequestMapping("/del")
    public  int del(Integer tr_id ){
        return service.del(tr_id);
    }
}
