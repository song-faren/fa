package com.zb.pojo;

import lombok.Data;

//旅游路线表
@Data
public class Tour_route {
    private Integer tr_id;//旅游路线编号
    private String tr_name;// 旅游线路名称
    private Integer tr_type;// 类型 1 自由行 2纯玩闭
    private Integer tr_price;// 价格
    private String tr_phone;// 咨询电话
    private String tr_user;//  咨询人
    private String create_time;// 创建日期

}
