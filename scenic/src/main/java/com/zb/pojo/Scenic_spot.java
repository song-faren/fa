package com.zb.pojo;

import lombok.Data;

//景点表
@Data
public class Scenic_spot {
    private Integer ss_id;// 景点编号
    private String ss_name;//景点名称
    private String ss_duration;// 建议游玩时长
    private String ss_content;// 景点简介
}
