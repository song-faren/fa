package com.zb.mapper;

import com.zb.pojo.Scenic_spot;
import com.zb.pojo.Tour_route;
import com.zb.pojo.Tour_scenic_relation;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface Tour_routeMapper {
    public List<Tour_route> getTour_route();
    public int getAdd(Tour_route tour_route);
    public  int del(Integer tr_id );
    public Scenic_spot selScenic_spot(Integer ss_id );

    public Tour_scenic_relation selrelation(Integer tr_id);

}
