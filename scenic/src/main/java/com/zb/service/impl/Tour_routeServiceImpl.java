package com.zb.service.impl;

import com.zb.mapper.Tour_routeMapper;
import com.zb.pojo.Scenic_spot;
import com.zb.pojo.Tour_route;
import com.zb.pojo.Tour_scenic_relation;
import com.zb.service.Tour_routeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class Tour_routeServiceImpl implements Tour_routeService {
    @Resource
    Tour_routeMapper mapper;
    @Override
    public List<Tour_route> getTour_route() {
        return mapper.getTour_route();
    }

    @Override
    public int getAdd(Tour_route tour_route) {
        return mapper.getAdd(tour_route);
    }

    @Override
    public int del(Integer tr_id) {
        return mapper.del(tr_id);
    }
    public Scenic_spot selScenic_spot(Integer ss_id ){
        return mapper.selScenic_spot(ss_id);
    }


}
