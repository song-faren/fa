package com.zb.service;

import com.zb.pojo.Scenic_spot;
import com.zb.pojo.Tour_route;
import com.zb.pojo.Tour_scenic_relation;

import java.util.List;

public interface Tour_routeService {
    public List<Tour_route> getTour_route();
    //添加
    public int getAdd(Tour_route tour_route);
    //删除
    public  int del(Integer tr_id );
    //详情
    public Scenic_spot selScenic_spot(Integer ss_id );

}
